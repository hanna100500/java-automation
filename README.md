Мурашко Анна Александровна

- *Город: Минск*
- *Возраст: 33*
- *Образование: инженер*
- *Место работы: EPAM*
- *Должность: Software Testing Engineer*
- *Мотивация: expand expertise*
- *anna.muraschko@gmail.com*
- *+375291518581*

Links to homeworks:

1. [java_intro](./homeworks/java_intro/README.md)
2. [testing](./homeworks/testing/README.md)
3. [unit_testing_framework](./homeworks/unit_testing_framework/README.md)
4. [db](./homeworks/db/README.md)
5. [jdbc_xml](./homeworks/jdbc_xml/README.md)


